import pickle
import tensorflow as tf
import numpy as np
from utils.DatasetReader import load_inria_aerial_dataset_generator
from utils.Convnet import *
import sys
from time import perf_counter
import os

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="3"  # specify which GPU(s) to be used

###################
#    LOAD_DATA    #
###################
DSPATH = '/home/jordi/inriadataset/AerialImageDataset/100x100'
#CITY = 'tyrol-w'
CITY = ''

print('Loading Pickles')

print('Loading X Train')
with open(os.path.join(DSPATH, 'xtrain' + CITY + '.pkl'), 'rb') as f:
    xtrain = pickle.load(f)

print('Loading Y Train')
with open(os.path.join(DSPATH, 'ytrain' + CITY + '.pkl'), 'rb') as f:
    ytrain = pickle.load(f)

print('Loading X Val')
with open(os.path.join(DSPATH, 'xval' + CITY + '.pkl'), 'rb') as f:
    xval = pickle.load(f)

print('Loading Y Val')
with open(os.path.join(DSPATH, 'yval' + CITY + '.pkl'), 'rb') as f:
    yval = pickle.load(f)


###################
#    DEFINE CNN   #
###################
img_w = xtrain[0].shape[0]
img_h = xtrain[0].shape[1]
n_chan = xtrain[0].shape[2]

num_positive = np.sum(ytrain)
num_labels = len(ytrain) * img_w * img_h
positive_rate = num_positive / num_labels
loss_unbalance_correction_factor = (1-positive_rate) / positive_rate
#loss_unbalance_correction_factor = 4
print("Positive rate: " + str(positive_rate) + "\tCorrection Factor:" + str(loss_unbalance_correction_factor))

tf.reset_default_graph()

with tf.name_scope("Input"):
    x = tf.placeholder(dtype=tf.float32, shape=[None, img_w, img_h, n_chan], name="X")
    y = tf.placeholder(dtype=tf.float32, shape=[None, img_w, img_h], name="Y")

conv1 = tf.layers.conv2d(inputs=x, filters=4, kernel_size=32, padding="same", activation="relu", name="Conv1")
gdn1 = tf.contrib.layers.gdn(inputs=conv1)
maxpool1 = tf.layers.max_pooling2d(inputs=gdn1, pool_size=32, strides=1, name="MaxPool1")
conv2 = tf.layers.conv2d(inputs=maxpool1, filters=8, kernel_size=32, padding="same", activation="relu", name="Conv2")
maxpool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=32, strides=1, name="MaxPool2")
conv3 = tf.layers.conv2d(inputs=maxpool2, filters=16, kernel_size=32, padding="same", activation="relu", name="Conv3")
maxpool3 = tf.layers.max_pooling2d(inputs=conv3, pool_size=16, strides=1, name="MaxPool3")
conv4 = tf.layers.conv2d(inputs=maxpool3, filters=32, kernel_size=32, padding="same", activation="relu", name="Conv4")
maxpool4 = tf.layers.max_pooling2d(inputs=conv4, pool_size=16, strides=1, name="MaxPool4")

drp1 = tf.layers.dropout(inputs=maxpool4, rate=0.5)

tconv4 = tf.layers.conv2d_transpose(inputs=drp1, filters=32, kernel_size=16, name="TConv4")
up4 = tf.concat([tconv4, conv4], axis=-1)
tconv3 = tf.layers.conv2d_transpose(inputs=up4, filters=16, kernel_size=16, name="TConv3")
up3 = tf.concat([tconv3, conv3], axis=-1)
tconv2 = tf.layers.conv2d_transpose(inputs=up3, filters=8, kernel_size=32, name="TConv2")
up2 = tf.concat([tconv2, conv2], axis=-1)
tconv1 = tf.layers.conv2d_transpose(inputs=up2, filters=4, kernel_size=32, name="TConv1")
up1 = tf.concat([tconv1, conv1], axis=-1)

conv_out = tf.reduce_max(tf.layers.conv2d(inputs=up1, filters=1, kernel_size=1, name="Conv5"), axis=-1)  # NO SOFTMAX, LOGITS!!!

#############################
#    DEFINE TF OPERATIONS   #
#############################

with tf.variable_scope("Train"):
    with tf.variable_scope("Predict"):
        predict = tf.round(tf.nn.sigmoid(conv_out))
    with tf.variable_scope('Loss'):
        loss = tf.nn.weighted_cross_entropy_with_logits(y, conv_out, loss_unbalance_correction_factor)
        mean_loss = tf.reduce_mean(loss)
    with tf.variable_scope('Accuracy'):
        is_correct_pred = tf.cast(tf.equal(predict, y), dtype=tf.float32)
        acc = tf.reduce_mean(is_correct_pred, name='accuracy')
    with tf.variable_scope('IoU'):
        mean_iou_op, update_conf_mat = tf.metrics.mean_iou(y, predict, num_classes=2)
    with tf.variable_scope("Optimizer"):
        optimizer = tf.train.AdamOptimizer(name='optimizer', learning_rate=0.0001).minimize(loss)

with tf.name_scope('Init'):
    init = tf.global_variables_initializer()
with tf.name_scope('Saver'):
    saver = tf.train.Saver(max_to_keep=100000)

#############################
#       TRAINING LOOP       #
#############################

PROJECTNAME = 'inria_unet_gdn'


def save_metrics(metricname, arr):
    with open(PROJECTNAME + '_' + metricname + '.txt', 'w') as f:
        for item in arr:
            f.write(str(item) + '\n')


EPOCHS = 200
BATCH_SIZE = 250

config = tf.ConfigProto()
config.gpu_options.allow_growth = True

run_options = tf.RunOptions(report_tensor_allocations_upon_oom=True)

accs_train = []
losses_train = []
ious_train = []

accs_val = []
losses_val = []
ious_val = []

epochs_time = []

num_batches_train = len(xtrain) // BATCH_SIZE
num_batches_val = len(xval) // BATCH_SIZE

from pympler.tracker import SummaryTracker
tracker = None

with tf.Session(config=config) as sess:
    sess.run(init, options=run_options)
    for epoch in range(EPOCHS):
        t_start_epoch = perf_counter()
        accs_epoch = []
        losses_epoch = []

        # Initialize confusion matrix and iou
        iou_mean_epoch = 0
        cm_total_epoch = None
        sess.run(tf.local_variables_initializer())

        if tracker is None:
            tracker = SummaryTracker()
        else:
            tracker.print_diff()
            del tracker
            tracker = SummaryTracker()

        for batch in range(num_batches_train):
            x_batch = xtrain[(batch * BATCH_SIZE): ((batch+1) * BATCH_SIZE)]
            y_batch = ytrain[(batch * BATCH_SIZE): ((batch+1) * BATCH_SIZE)]
            _, acc_batch, loss_batch = sess.run([optimizer, acc, mean_loss], options=run_options, feed_dict={x: x_batch, y: y_batch})

            del cm_total_epoch
            del iou_mean_epoch
            cm_total_epoch = sess.run(update_conf_mat, options=run_options, feed_dict={x: x_batch, y: y_batch})
            iou_mean_epoch = sess.run(mean_iou_op, options=run_options)

            accs_epoch.append(acc_batch)
            losses_epoch.append(loss_batch)

            i = "Epoch {0} ({1}/{2})    acc={3}    loss={4}    acc_batch={5}    loss_batch={6}    IoU={7}".format(epoch, batch, num_batches_train, np.mean(accs_epoch), np.mean(losses_epoch), acc_batch, loss_batch, iou_mean_epoch)
            print(i, end="\r")
            batch += 1

        print("\nConfussion Matrix for training epoch " + str(epoch) + ":")
        print(cm_total_epoch)

        acc_mean_epoch = np.mean(accs_epoch)
        accs_train.append(acc_mean_epoch)

        loss_mean_epoch = np.mean(losses_epoch)
        losses_train.append(loss_mean_epoch)

        ious_train.append(iou_mean_epoch)

        t_end_epoch = perf_counter()

        accs_val_batch = []
        losses_val_batch = []

        # Initialize confusion matrix and iou
        sess.run(tf.local_variables_initializer())
        iou_mean_val = 0
        cm_total_val = None

        for batch in range(num_batches_val):
            x_val_batch = xval[(batch * BATCH_SIZE): ((batch+1) * BATCH_SIZE)]
            y_val_batch = yval[(batch * BATCH_SIZE): ((batch+1) * BATCH_SIZE)]
            acc_val_batch, loss_val_batch = sess.run([acc, mean_loss], feed_dict={x: x_val_batch, y: y_val_batch})

            del cm_total_val
            del iou_mean_val
            cm_total_val = sess.run(update_conf_mat, options=run_options, feed_dict={x: x_val_batch, y: y_val_batch})
            iou_mean_val = sess.run(mean_iou_op, options=run_options)

            accs_val_batch.append(acc_val_batch)
            losses_val_batch.append(loss_val_batch)

            i = "Validating E{0} ({1}/{2})    acc={3}    loss={4}    acc_batch={5}    loss_batch={6}    IoU={7}".format(epoch, batch, num_batches_val, np.mean(accs_val_batch), np.mean(losses_val_batch), acc_val_batch, loss_val_batch, iou_mean_val)

            print(i, end="\r")
            batch += 1

        print("\nConfussion Matrix for validation epoch " + str(epoch) + ":")
        print(cm_total_val)
        
        acc_val_mean = np.mean(accs_val_batch)
        loss_val_mean = np.mean(losses_val_batch)

        epoch_time = (t_end_epoch - t_start_epoch)

        accs_val.append(acc_val_mean)
        losses_val.append(loss_val_mean)
        ious_val.append(iou_mean_val)
        epochs_time.append(epoch_time)

        i = "\nFINISHED EPOCH {0}:    time={1}s    acc={2}    loss={3}    IoU={4}    acc_val={5}    loss_val={6}    IoU_val={7}".format(epoch, epoch_time, acc_mean_epoch, loss_mean_epoch, iou_mean_epoch, acc_val_mean, loss_val_mean, iou_mean_val)
        print(i)

        save_metrics('acc_train', accs_train)
        save_metrics('loss_train', losses_train)
        save_metrics('iou_train', ious_train)
        save_metrics('acc_val', accs_val)
        save_metrics('loss_val', losses_val)
        save_metrics('iou_val', ious_val)
        save_metrics('train_time', epochs_time)

        save_path = saver.save(sess, "cloudmodel_" + PROJECTNAME + "_" + str(epoch) + ".ckpt")
        print("Model " + str(epoch) + " saved at: %s" % save_path)

