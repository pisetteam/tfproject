from keras.models import Sequential
from keras.optimizers import Adam
from keras.layers import Dense, Activation, Input, Conv2D, MaxPooling2D, Conv2DTranspose, UpSampling2D, Dropout, concatenate
from keras.models import Model
import numpy as np
from utils.DatasetReader import load_cloudmasks, crop_image_dataset, split_dataset
from utils.Convnet import *
import sys
import os

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"  # specify which GPU(s) to be used

###################
#    LOAD_DATA    #
###################

x_ds, y_ds = load_cloudmasks(path='./cloudmasks')
x_ds, y_ds = crop_image_dataset(x_ds, y_ds, random_order=True, crops=2)
xtrain, ytrain, xtest, ytest = split_dataset(x_ds, y_ds, ratio=0.15)
xtrain, ytrain, xval, yval = split_dataset(xtrain, ytrain, ratio=0.15)

###################
#    DEFINE CNN   #
###################

img_w = xtrain.shape[1]
img_h = xtrain.shape[2]
n_chan = xtrain.shape[3]
n_classes = ytrain.shape[3]


'''
SIMPLE U-NET ???
'''
inputs = Input((img_w, img_h, n_chan))

conv1 = Conv2D(filters=5, kernel_size=32, padding='same', activation='relu', input_shape=(img_w, img_h, n_chan))(inputs)
maxp1 = MaxPooling2D(pool_size=32, strides=1)(conv1)
conv2 = Conv2D(filters=10, kernel_size=64, padding='same', activation='relu')(maxp1)
maxp2 = MaxPooling2D(pool_size=64, strides=1)(conv2)

drp = Dropout(rate=0.5)(maxp2)

tconv1 = Conv2DTranspose(filters=10, kernel_size=64)(drp)
up1 = concatenate([tconv1, conv2])
tconv2 = Conv2DTranspose(filters=5, kernel_size=32)(up1)
up2 = concatenate([tconv2, conv1])
output = Conv2D(filters=n_classes, kernel_size=16, padding='same', activation='softmax')(up2)

model = Model(input=inputs, output=output)
model.summary()
model.compile(loss='categorical_crossentropy', optimizer=Adam(), metrics=['acc'])

model.fit(xtrain, ytrain, batch_size=16, epochs=30)
model.save(os.path.join(os.getcwd(), 'cloudmodel.h5'))


'''
SIMPLE FCN NICE TRY :(

model = Sequential()
model.add(Conv2D(filters=32, kernel_size=32, padding='same', activation='relu', input_shape=(img_w, img_h, n_chan)))
model.add(MaxPooling2D(pool_size=32, strides=1))
model.add(Conv2D(filters=64, kernel_size=64, padding='same', activation='relu'))
model.add(MaxPooling2D(pool_size=64, strides=1))
#model.add(Dropout(rate=0.5))
model.add(Conv2DTranspose(filters=64, kernel_size=64))
model.add(Conv2DTranspose(filters=n_classes, kernel_size=32))
#model.add(Conv2D(filters=n_classes, kernel_size=1))
model.summary()

model.compile(loss='categorical_crossentropy', optimizer=Adam(), metrics=['acc'])

model.fit(xtrain[:10], ytrain[:10], batch_size=2, epochs=20)
'''




'''
BIG U-NET

inputs = Input((img_w, img_h, n_chan))
conv1 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(inputs)
conv1 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv1)
pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
conv2 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool1)
conv2 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv2)
pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
conv3 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool2)
conv3 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv3)
pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
conv4 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool3)
conv4 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv4)
drop4 = Dropout(0.5)(conv4)
pool4 = MaxPooling2D(pool_size=(2, 2))(drop4)

conv5 = Conv2D(1024, 3, activation='relu', padding='same', kernel_initializer='he_normal')(pool4)
conv5 = Conv2D(1024, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv5)
drop5 = Dropout(0.5)(conv5)

up6 = Conv2D(512, 2, activation='relu', padding='causal', kernel_initializer='he_normal')(UpSampling2D(size=(2, 2))(drop5))
merge6 = concatenate([drop4, up6], axis=3)
conv6 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge6)
conv6 = Conv2D(512, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv6)

up7=Conv2D(256, 2, activation='relu', padding='same', kernel_initializer='he_normal')(UpSampling2D(size=(2, 2))(conv6))
merge7 = concatenate([conv3, up7], axis=3)
conv7 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge7)
conv7 = Conv2D(256, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv7)

up8 = Conv2D(128, 2, activation='relu', padding='same', kernel_initializer='he_normal')(UpSampling2D(size=(2,2))(conv7))
merge8 = concatenate([conv2,up8], axis=3)
conv8 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge8)
conv8 = Conv2D(128, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv8)

up9 = Conv2D(64, 2, activation='relu', padding='same', kernel_initializer='he_normal')(UpSampling2D(size=(2,2))(conv8))
merge9 = concatenate([conv1,up9], axis=3)
conv9 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(merge9)
conv9 = Conv2D(64, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv9)
conv9 = Conv2D(2, 3, activation='relu', padding='same', kernel_initializer='he_normal')(conv9)
conv10 = Conv2D(1, 1, activation='sigmoid')(conv9)

model = Model(input=inputs, output=conv10)

model.compile(optimizer=Adam(lr=1e-4), loss='categorical_crossentropy', metrics=['accuracy'])

model.summary()
'''
