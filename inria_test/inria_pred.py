import pickle
import tensorflow as tf
import numpy as np
from utils.Convnet import *
from utils.DatasetReader import load_inria_aerial_dataset_generator
import sys
import os
from PIL import Image

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="2"  # specify which GPU(s) to be used

###################
#    LOAD_DATA    #
###################
DSPATH = '/home/jordi/inriadataset/AerialImageDataset/100x100'

traingen, valgen, num_images_train, num_images_val = load_inria_aerial_dataset_generator(path=DSPATH, labels_one_hot=False)

###################
#    DEFINE CNN   #
###################
for x,y in traingen():
    img_w = x.shape[1]
    img_h = x.shape[2]
    n_chan = x.shape[3]
    break

loss_unbalance_correction_factor = 5.209787220799553

with tf.name_scope("Input"):
    x = tf.placeholder(dtype=tf.float32, shape=[None, img_w, img_h, n_chan], name="X")
    y = tf.placeholder(dtype=tf.float32, shape=[None, img_w, img_h], name="Y")

conv1 = tf.layers.conv2d(inputs=x, filters=4, kernel_size=8, padding="same", activation="relu", name="Conv1")
maxpool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=8, strides=1, name="MaxPool1")
conv2 = tf.layers.conv2d(inputs=maxpool1, filters=4, kernel_size=8, padding="same", activation="relu", name="Conv2")
maxpool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=8, strides=1, name="MaxPool2")
conv3 = tf.layers.conv2d(inputs=maxpool2, filters=8, kernel_size=4, padding="same", activation="relu", name="Conv3")
maxpool3 = tf.layers.max_pooling2d(inputs=conv3, pool_size=4, strides=1, name="MaxPool3")
conv4 = tf.layers.conv2d(inputs=maxpool3, filters=16, kernel_size=4, padding="same", activation="relu", name="Conv4")
maxpool4 = tf.layers.max_pooling2d(inputs=conv4, pool_size=4, strides=1, name="MaxPool4")

drp1 = tf.layers.dropout(inputs=maxpool4, rate=0.5)

tconv4 = tf.layers.conv2d_transpose(inputs=drp1, filters=16, kernel_size=4, name="TConv4")
up4 = tf.concat([tconv4, conv4], axis=-1)
tconv3 = tf.layers.conv2d_transpose(inputs=up4, filters=8, kernel_size=4, name="TConv3")
up3 = tf.concat([tconv3, conv3], axis=-1)
tconv2 = tf.layers.conv2d_transpose(inputs=up3, filters=4, kernel_size=8, name="TConv2")
up2 = tf.concat([tconv2, conv2], axis=-1)
tconv1 = tf.layers.conv2d_transpose(inputs=up2, filters=4, kernel_size=8, name="TConv1")
up1 = tf.concat([tconv1, conv1], axis=-1)
conv_out = tf.reduce_max(tf.layers.conv2d(inputs=up1, filters=1, kernel_size=1, name="Conv5"), axis=-1)  # NO SOFTMAX, LOGITS!!!

#############################
#    DEFINE TF OPERATIONS   #
#############################

with tf.variable_scope("Train"):
    with tf.variable_scope("Predict"):
        predict = tf.round(tf.nn.sigmoid(conv_out))
    with tf.variable_scope('Loss'):
        loss = tf.nn.weighted_cross_entropy_with_logits(y, conv_out, loss_unbalance_correction_factor)
        mean_loss = tf.reduce_mean(loss)
    with tf.variable_scope('Accuracy'):
        is_correct_pred = tf.cast(tf.equal(predict, y), dtype=tf.float32)
        acc = tf.reduce_mean(is_correct_pred, name='accuracy')
    with tf.variable_scope('IoU'):
        mean_iou_op, update_conf_mat = tf.metrics.mean_iou(y, predict, num_classes=2)
    with tf.variable_scope("Optimizer"):
        optimizer = tf.train.AdamOptimizer(name='optimizer', learning_rate=0.0001).minimize(loss)

with tf.name_scope('Init'):
    init = tf.global_variables_initializer()
with tf.name_scope('Saver'):
    saver = tf.train.Saver(max_to_keep=100000)

#############################
#       TRAINING LOOP       #
#############################

PROJECTNAME = 'inria_unet_sbase'
SHORTPROJECTNAME = 'superBASE'

BATCH_SIZE = 250

config = tf.ConfigProto()
config.gpu_options.allow_growth = True

run_options = tf.RunOptions(report_tensor_allocations_upon_oom=True)

num_batches_val = num_images_val // BATCH_SIZE

with tf.Session(config=config) as sess:
    sess.run(init, options=run_options)
    for epoch in [150]: #range(150):
        saver.restore(sess, "../inria_" + SHORTPROJECTNAME + "/cloudmodel_" + PROJECTNAME + "_" + str(epoch) + ".ckpt")
        print("Loaded model " + SHORTPROJECTNAME + " - " + str(epoch))

        accs_val_batch = []
        losses_val_batch = []

        # Initialize confusion matrix and iou
        sess.run(tf.local_variables_initializer())
        iou_mean_val = 0
        cm_total_val = None

        batch = 1
        for x_val_batch, y_val_batch in valgen(BATCH_SIZE):
            acc_val_batch, loss_val_batch, pred = sess.run([acc, mean_loss, predict], feed_dict={x: x_val_batch, y: y_val_batch})
            del cm_total_val
            del iou_mean_val
            cm_total_val = sess.run(update_conf_mat, options=run_options, feed_dict={x: x_val_batch, y: y_val_batch})
            iou_mean_val = sess.run(mean_iou_op, options=run_options)

            accs_val_batch.append(acc_val_batch)
            losses_val_batch.append(loss_val_batch)

            i = "Validating E{0} ({1}/{2})    acc={3}    loss={4}    acc_batch={5}    loss_batch={6}    IoU={7}".format(
                epoch, batch, num_batches_val, np.mean(accs_val_batch), np.mean(losses_val_batch), acc_val_batch,
                loss_val_batch, iou_mean_val)

            print(i, end="\r")

            if(batch == 1):
                for i in range(len(pred)):
                    Image.fromarray(pred[i]).save(os.path.join('preds' + SHORTPROJECTNAME, 'pred_I' + str(i) + '_E' + str(epoch) + '_B' + str(batch) + '.tif'), 'tiff')
                    Image.fromarray(y_val_batch[i]).save(os.path.join('gt' + SHORTPROJECTNAME, 'gt_E' + str(i) + '_E' + str(epoch) + '_B' + str(batch) + '.tif'), 'tiff')
                    Image.fromarray((x_val_batch[i] * 255).astype('uint8')).save(os.path.join('img' + SHORTPROJECTNAME, 'img_I' + str(i) + '.png'), 'png')
            batch += 1

        print("\nConfussion Matrix for validation epoch " + str(epoch) + ":")
        print(cm_total_val)

        acc_val_mean = np.mean(accs_val_batch)
        loss_val_mean = np.mean(losses_val_batch)

        i = "\nFINISHED EPOCH {0}: acc_val={1}    loss_val={2}    IoU_val={3}".format(epoch, acc_val_mean, loss_val_mean, iou_mean_val)
        print(i)
