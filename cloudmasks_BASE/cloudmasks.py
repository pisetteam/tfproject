import pickle
import tensorflow as tf
import numpy as np
from utils.DatasetReader import load_cloudmasks, crop_image_dataset, split_dataset
from utils.Convnet import *
import sys
from time import perf_counter
import os

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="2"  # specify which GPU(s) to be used

###################
#    LOAD_DATA    #
###################

x_ds, y_ds = load_cloudmasks(path='d:/cloudmasks')

yshape = y_ds.shape
y_ds2 = np.zeros((yshape[0], yshape[1], yshape[2], 2))
for i in range(yshape[0]):
    for h in range(yshape[1]):
        for w in range(yshape[2]):
            if y_ds[i, h, w, 5] == 1:
                y_ds2[i, h, w] = [0, 1]
            else:
                y_ds2[i, h, w] = [1, 0]

x_ds, y_ds2 = crop_image_dataset(x_ds, y_ds2, random_order=True, crops=2)
xtrain, ytrain, xtest, ytest = split_dataset(x_ds, y_ds2, ratio=0.15)
xtrain, ytrain, xval, yval = split_dataset(xtrain, ytrain, ratio=0.15)

###################
#    DEFINE CNN   #
###################

img_w = xtrain.shape[1]
img_h = xtrain.shape[2]
n_chan = xtrain.shape[3]
n_classes = ytrain.shape[3]

tf.reset_default_graph()

with tf.name_scope("Input"):
    x = tf.placeholder(dtype=tf.float32, shape=[None, img_w, img_h, n_chan], name="X")
    y = tf.placeholder(dtype=tf.float32, shape=[None, img_w, img_h, n_classes], name="Y")

conv1 = tf.layers.conv2d(inputs=x, filters=4, kernel_size=32, padding="same", activation="relu", name="Conv1")
maxpool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=32, strides=1, name="MaxPool1")
conv2 = tf.layers.conv2d(inputs=maxpool1, filters=8, kernel_size=32, padding="same", activation="relu", name="Conv2")
maxpool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=32, strides=1, name="MaxPool2")
conv3 = tf.layers.conv2d(inputs=maxpool2, filters=16, kernel_size=32, padding="same", activation="relu", name="Conv3")
maxpool3 = tf.layers.max_pooling2d(inputs=conv3, pool_size=32, strides=1, name="MaxPool3")
conv4 = tf.layers.conv2d(inputs=maxpool3, filters=32, kernel_size=32, padding="same", activation="relu", name="Conv4")
maxpool4 = tf.layers.max_pooling2d(inputs=conv4, pool_size=32, strides=1, name="MaxPool4")

drp1 = tf.layers.dropout(inputs=maxpool4, rate=0.5)

tconv4 = tf.layers.conv2d_transpose(inputs=drp1, filters=32, kernel_size=32, name="TConv4")
up4 = tf.concat([tconv4, conv4], axis=-1)
tconv3 = tf.layers.conv2d_transpose(inputs=up4, filters=16, kernel_size=32, name="TConv3")
up3 = tf.concat([tconv3, conv3], axis=-1)
tconv2 = tf.layers.conv2d_transpose(inputs=up3, filters=8, kernel_size=32, name="TConv2")
up2 = tf.concat([tconv2, conv2], axis=-1)
tconv1 = tf.layers.conv2d_transpose(inputs=up2, filters=4, kernel_size=32, name="TConv1")
up1 = tf.concat([tconv1, conv1], axis=-1)

conv_out = tf.layers.conv2d(inputs=up1, filters=7, kernel_size=1, name="Conv5") # NO SOFTMAX, LOGITS!!!

#############################
#    DEFINE TF OPERATIONS   #
#############################

with tf.variable_scope("Train"):
    with tf.variable_scope("Predict"):
        predict = tf.argmax(tf.nn.softmax(logits=conv_out, axis=3), axis=3)
    with tf.variable_scope('Loss'):
        loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=conv_out, labels=y), name='loss')
    with tf.variable_scope('Accuracy'):
        is_correct_pred = tf.equal(predict, tf.argmax(y, axis=3))
        acc = tf.reduce_mean(tf.cast(is_correct_pred, dtype=tf.float32), name='accuracy')
    with tf.variable_scope("Optimizer"):
        optimizer = tf.train.AdamOptimizer(name='optimizer').minimize(loss)

with tf.name_scope('Init'):
    init = tf.global_variables_initializer()
with tf.name_scope('Saver'):
    saver = tf.train.Saver()


#############################
#       TRAINING LOOP       #
#############################

PROJECTNAME = 'cloudmasks_unet'
def save_metrics(metricname, arr):
    with open(PROJECTNAME + '_' + metricname + '.txt', 'w') as f:
        for item in arr:
            f.write(str(item) + '\n')

EPOCHS = 50
BATCH_SIZE = 2

config = tf.ConfigProto()
config.gpu_options.allow_growth = True

run_options = tf.RunOptions(report_tensor_allocations_upon_oom=True)

accs_train = []
losses_train = []

accs_val = []
losses_val = []

epochs_time = []

with tf.Session(config=config) as sess:
    sess.run(init, options=run_options)
    for epoch in range(EPOCHS):
        t_start_epoch = perf_counter()
        accs_epoch = []
        losses_epoch = []
        for batch in range(xtrain.shape[0]//BATCH_SIZE):
            x_batch = xtrain[batch*BATCH_SIZE: (batch+1)*BATCH_SIZE]
            y_batch = ytrain[batch*BATCH_SIZE: (batch+1)*BATCH_SIZE]

            _, acc_batch, loss_batch = sess.run([optimizer, acc, loss], options=run_options, feed_dict={x: x_batch, y: y_batch})

            accs_epoch.append(acc_batch)
            losses_epoch.append(loss_batch)

            i = "Epoch {0} ({1}/{2})\tacc={3}\tloss={4}".format(epoch, batch, xtrain.shape[0]//BATCH_SIZE, np.mean(accs_epoch), np.mean(losses_epoch))
            sys.stdout.write("\r" + i)
            sys.stdout.flush()

        acc_mean_epoch = np.mean(accs_epoch)
        accs_train.append(acc_mean_epoch)

        loss_mean_epoch = np.mean(losses_epoch)
        losses_train.append(loss_mean_epoch)
        t_end_epoch = perf_counter()
        accs_val_batch = []
        losses_val_batch = []
        for batch in range(xval.shape[0] // BATCH_SIZE):
            x_val_batch = xval[batch * BATCH_SIZE: (batch + 1) * BATCH_SIZE]
            y_val_batch = yval[batch * BATCH_SIZE: (batch + 1) * BATCH_SIZE]

            acc_val_batch, loss_val_batch = sess.run([tf.reduce_mean(acc), tf.reduce_mean(loss)], feed_dict={x: x_val_batch, y: y_val_batch})

            accs_val_batch.append(acc_val_batch)
            losses_val_batch.append(loss_val_batch)

        acc_val_mean = np.mean(acc_val_batch)
        loss_val_mean = np.mean(loss_val_batch)
        epoch_time = (t_end_epoch - t_start_epoch)

        accs_val.append(acc_val_mean)
        losses_val.append(loss_val_mean)
        epochs_time.append(epoch_time)

        print("\nFINISHED EPOCH {0}:\ttime={1}s\tacc={2}\tloss={3}\tacc_val={4}\tloss_val={5}".format(epoch, epoch_time, acc_mean_epoch, loss_mean_epoch, acc_val_mean, loss_val_mean))

        save_metrics('acc_train', accs_train)
        save_metrics('loss_train', losses_train)
        save_metrics('acc_val', accs_val)
        save_metrics('loss_val', losses_val)
        save_metrics('train_time', epochs_time)

        save_path = saver.save(sess, "cloudmodel_" + PROJECTNAME + "_" + str(epoch) + ".ckpt")
        print("Model " + str(epoch) + " saved at: %s" % save_path)

    accs_test_batch = []
    losses_test_batch = []
    for batch in range(xtest.shape[0] // BATCH_SIZE):
        x_test_batch = xtest[batch * BATCH_SIZE: (batch + 1) * BATCH_SIZE]
        y_test_batch = ytest[batch * BATCH_SIZE: (batch + 1) * BATCH_SIZE]

        acc_test_batch, loss_test_batch = sess.run([tf.reduce_mean(acc), tf.reduce_mean(loss)], feed_dict={x: x_test_batch, y: y_test_batch})

        accs_test_batch.append(acc_test_batch)
        losses_test_batch.append(loss_test_batch)

    acc_test = np.mean(accs_test_batch)
    loss_test = np.mean(losses_test_batch)
    print("\nFINISHED TRAINING:\tacc_test={1}\tloss_test={2}".format(epoch, acc_test, loss_test))
    save_metrics('acc_test', [acc_test])
    save_metrics('loss_test', [loss_test])
