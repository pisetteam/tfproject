import numpy as np
import os
import skimage
import pickle
import re
from PIL import Image
import matplotlib.pyplot as plt

def loadCifar(directory):
    with open(os.path.join(directory, 'batches.meta'), 'rb') as meta:
        metadict = pickle.load(meta, encoding='bytes')

    def load_pickle_file_list(files):
        _img_w = 32
        _img_h = 32
        _n_chan = 3

        x = np.array([], dtype=np.float32).reshape((0, _img_w, _img_h, _n_chan))
        y = np.array([], dtype=np.int)

        for f in files:
            with open(os.path.join(directory, f), 'rb') as batch:
                datadict = pickle.load(batch, encoding='bytes')
                print('loading ' + datadict[b'batch_label'].decode('utf-8') + '...')
                x = np.append(x, datadict[b'data'].reshape((metadict[b'num_cases_per_batch'], _img_h, _img_w, _n_chan), order="F").transpose((0, 2, 1, 3)), axis=0)
                y = np.append(y, datadict[b'labels'], axis=0)

        _max_y = y.max()

        def as_dummy(lbl):
            res = np.zeros(_max_y + 1)
            res[lbl] = 1
            return res

        y = np.array([as_dummy(lbl) for lbl in y])

        return x/255, y

    train_files = [f for f in os.listdir(directory) if f.startswith('data_batch')]
    test_files = [f for f in os.listdir(directory) if f.startswith('test')]

    xtrain, ytrain = load_pickle_file_list(train_files)
    xtest, ytest = load_pickle_file_list(test_files)

    return xtrain, ytrain, xtest, ytest

def load_cifar_100(directory="./cifar-100-python"):
    with open(os.path.join(directory, 'meta'), 'rb') as meta:
        metadict = pickle.load(meta, encoding='bytes')

    with open(os.path.join(directory, 'train'), 'rb') as f:
        train = pickle.load(f, encoding="bytes")

    with open(os.path.join(directory, 'test'), 'rb') as f:
        test = pickle.load(f, encoding="bytes")

    xtrain = train[b'data'].reshape((50000, 32, 32, 3), order="F").transpose((0, 2, 1, 3))
    ytrain = np.array(train[b'fine_labels'])

    xtest = test[b'data'].reshape((10000, 32, 32, 3), order="F").transpose((0, 2, 1, 3))
    ytest = np.array(test[b'fine_labels'])

    def one_hot(num, nclasses):
        res = np.zeros(nclasses)
        res[num] = 1
        return res

    ytrain = np.array([one_hot(_y, 100) for _y in ytrain])
    ytest = np.array([one_hot(_y, 100) for _y in ytest])

    return xtrain, ytrain, xtest, ytest

def load_image_dataset(directory, img_shape=(150, 150), labels_as_numeric=True):
    directory = os.path.abspath(directory)
    data = []
    labels = []
    subdirs = os.listdir(directory)

    print('loading image dataset...')

    for label in subdirs:
        imagespath = os.path.join(directory, label)
        for file in os.listdir(imagespath):
            filepath = os.path.join(imagespath, file)
            data.append(skimage.transform.resize(skimage.data.imread(filepath), img_shape))
            labels.append(label)

    print('detected ', len(subdirs), ' classes')

    if labels_as_numeric:
        if len(subdirs) < 2:
            raise(Exception("Cannot find 2 classes (at least)"))
        else:
            label_length = len(subdirs)

        labels = [[y == subdirs.index(l) for y in range(label_length)] for l in labels]

        return np.array(data).astype(np.float32), np.array(labels).astype(np.float32).reshape((len(labels), label_length))
    else:
        return np.array(data).astype(np.float32), np.array(labels).astype(np.float32)


def load_cloudmasks(path='./cloudmasks'):
    if os.path.exists(os.path.join(path, 'cloudmask_x.pkl')) and os.path.exists(os.path.join(path, 'cloudmask_y.pkl')):
        print("Loading dataset from pickles...")
        with open(os.path.join(path, 'cloudmask_x.pkl'), 'rb') as f:
            x = pickle.load(f)
        with open(os.path.join(path, 'cloudmask_y.pkl'), 'rb') as f:
            y = pickle.load(f)
    else:
        print("Loading dataset from original images...")
        files = [re.findall(r'(.*)_mask\.png', f)[0] for f in os.listdir(path) if re.match(r'.*_mask\.png', f)]

        x = []
        y_rgb = []

        for f in files:
            x.append(skimage.data.imread(os.path.join(path, f + '_photo.png')))
            y_rgb.append(skimage.data.imread(os.path.join(path, f + '_mask.png')))

        x = np.array(x)
        y_rgb = np.array(y_rgb)

        if os.path.exists(os.path.join(path, 'palette.pkl')):
            print('Loading palette from pickle...')
            with open(os.path.join(path, 'palette.pkl'), 'rb') as f:
                palette = pickle.load(f)
        else:
            print('Creating rgb-to-class palette. This may take some time...')
            palette = {tuple(b): a for (a, b) in enumerate(np.unique(np.array([col for sample in y_rgb for row in sample for col in row]), axis=0))}
            with open(os.path.join(path, 'palette.pkl'), 'wb') as f:
                pickle.dump(palette, f, pickle.HIGHEST_PROTOCOL)

        y_shape = list(y_rgb.shape[0:3])
        y_shape.insert(3, len(palette))
        y = np.zeros(tuple(y_shape), dtype=np.int)

        print('One-hot label enconding...')

        def one_hot(num, nclasses):
            return [int(_a == num) for _a in range(nclasses)]

        for i in range(y_shape[0]):
            for j in range(y_shape[1]):
                for k in range(y_shape[2]):
                    y[i][j][k] = one_hot(palette.get(tuple(y_rgb[i][j][k])), y_shape[3])

        print('Saving to pickle...')
        with open(os.path.join(path, 'cloudmask_x.pkl'), 'wb') as f:
            pickle.dump(x, f, pickle.HIGHEST_PROTOCOL)
        with open(os.path.join(path, 'cloudmask_y.pkl'), 'wb') as f:
            pickle.dump(y, f, pickle.HIGHEST_PROTOCOL)

    return np.array(x)/255, np.array(y)


def randomize(x, y=None, seed=666):
    np.random.seed(seed)
    idxs = np.random.permutation(len(x))

    x = [x[i] for i in idxs]
    if y is None:
        return x
    else:
        assert(len(x) == len(y))
        y = [y[i] for i in idxs]
        return x, y


def split_dataset(x, y, ratio=0.15):
    assert len(x) == len(y)
    idx = np.arange(len(x))
    idx_split = int(np.round(len(x) * (1 - ratio)))
    return x[idx[0:idx_split]], y[idx[0:idx_split]], x[idx[idx_split:len(x)]], y[idx[idx_split:len(x)]]


def crop_image_dataset(xtrain, ytrain, xtest=None, ytest=None, random_order=True, seed=666, crops=1):
    def crop_set(a, crop):
        w_split = int(a.shape[1] / 2)
        h_split = int(a.shape[2] / 2)
        res = np.array(np.array(a[:, :w_split, :h_split, :]))
        res = np.append(res, np.array(a[:, w_split:, :h_split, :]), axis=0)
        res = np.append(res, np.array(a[:, :w_split, h_split:, :]), axis=0)
        res = np.append(res, np.array(a[:, w_split:, h_split:, :]), axis=0)
        if (crop == 1):
            return res
        else:
            return crop_set(res, crop - 1)

    if(random_order):
        if(xtest is None or ytest is None):
            return randomize(crop_set(xtrain, crops), crop_set(ytrain, crops), seed)
        else:
            return randomize(crop_set(xtrain, crops), crop_set(ytrain, crops), seed), randomize(crop_set(xtest, crops), crop_set(ytest, crops), seed+1)
    else:
        if(xtest is None or ytest is None):
            return crop_set(xtrain, crops), crop_set(ytrain, crops)
        else:
            return crop_set(xtrain, crops), crop_set(ytrain, crops), crop_set(xtest, crops), crop_set(ytest, crops)

def crop_size(x, y, out_shape=(500,500)):
    shp = x.shape
    assert(shp[0] == y.shape[0])

    newx = []
    newy = []

    for i in range(shp[0]):
        j = 0
        while j < shp[1]:
            k = 0
            while k < shp[2]:
                newx.append(x[i, j:(j+out_shape[0]), k:(k+out_shape[1])])
                newy.append(y[i, j:(j + out_shape[0]), k:(k + out_shape[1])])
                k += out_shape[1]
            j += out_shape[0]

    return newx, newy


def load_inria_aerial_train_dataset(path='c:/Users/jordi/Downloads/AerialImageDataset/'):
    files = os.listdir(os.path.join(path, 'train', 'images'))

    x = []
    y = []

    for f in files:
        x.append(np.array(Image.open(os.path.join(path, 'train', 'images', f))))
        y.append(np.array(Image.open(os.path.join(path, 'train', 'gt', f))))

    val_idx = [re.match(r'[a-z|\-]+[0-5].tif', f) is not None for f in files]

    xval = np.array([x[i] for i in range(len(x)) if val_idx[i]]) / 255
    yval = np.array([y[i] for i in range(len(y)) if val_idx[i]], dtype=np.int) / 255
    x = np.array([x[i] for i in range(len(x)) if not val_idx[i]]) / 255
    y = np.array([y[i] for i in range(len(y)) if not val_idx[i]], dtype=np.int) / 255

    return x, y, xval, yval

def process_inria_aerial_train_dataset(path='c:/Users/jordi/Downloads/AerialImageDataset/', out_shape=(250, 250)):
    files = os.listdir(os.path.join(path, 'train', 'images'))

    processed_inria_base_path = os.path.join(path, str(out_shape[0]) + 'x' + str(out_shape[1]), 'train')
    processed_images_path = os.path.join(processed_inria_base_path, 'images')
    processed_gt_path = os.path.join(processed_inria_base_path, 'gt')

    if not os.path.exists(processed_inria_base_path):
        os.makedirs(processed_inria_base_path)
    if not os.path.exists(processed_images_path):
        os.mkdir(processed_images_path)
    if not os.path.exists(processed_gt_path):
        os.mkdir(processed_gt_path)

    for f in files:
        img_x = np.array(Image.open(os.path.join(path, 'train', 'images', f)))
        img_y = np.array(Image.open(os.path.join(path, 'train', 'gt', f)))

        assert(img_x.shape[0] == 5000 and img_x.shape[1] == 5000)

        j = 0
        while j < 5000:
            k = 0
            while k < 5000:
                newfname = f.split('.')[0] + '_' + str(j) + '_' + str(k) + '.tif'
                Image.fromarray(img_x[j:(j + out_shape[0]), k:(k + out_shape[1])]).save(os.path.join(processed_images_path, newfname), 'tiff')
                Image.fromarray(img_y[j:(j + out_shape[0]), k:(k + out_shape[1])]).save(os.path.join(processed_gt_path, newfname), 'tiff')
                k += out_shape[1]
            j += out_shape[0]


def load_inria_aerial_dataset_generator(path='c:/Users/jordi/Downloads/AerialImageDataset/250x250', labels_one_hot=False):
    files = randomize(os.listdir(os.path.join(path, 'train', 'images')))
    val_regex = r'[a-z|\-]+[0-5]\_[0-9]+\_[0-9]+.tif'
    train_files = [f for f in files if re.match(val_regex, f) is None]
    val_files = [f for f in files if re.match(val_regex, f) is not None]

    def one_hot(a, num_classes):
        if not labels_one_hot:
            return a

        b = np.zeros((a.shape[0], a.shape[1], num_classes))
        for i in range(a.shape[0]):
            for j in range(a.shape[1]):
                b[i,j] = [int(a[i,j] == c) for c in range(num_classes)]
        return b


    def train_generator(batch_size=1):
        i = 0
        while i < len(train_files):
            x_batch = []
            y_batch = []
            for ib in range(batch_size):
                f = train_files[i + ib]
                x_batch.append(np.array(Image.open(os.path.join(path, 'train', 'images', f))) / 255)
                y_batch.append(one_hot(np.array(Image.open(os.path.join(path, 'train', 'gt', f))) / 255, 2))
            i += batch_size
            yield np.array(x_batch), np.array(y_batch)

    def val_generator(batch_size=1):
        i = 0
        while i < len(val_files):
            x_batch = []
            y_batch = []
            for ib in range(batch_size):
                f = val_files[i + ib]
                x_batch.append(np.array(Image.open(os.path.join(path, 'train', 'images', f))) / 255)
                y_batch.append(one_hot(np.array(Image.open(os.path.join(path, 'train', 'gt', f))) / 255, 2))
            i += batch_size
            yield np.array(x_batch), np.array(y_batch)

    return train_generator, val_generator, len(train_files), len(val_files)
