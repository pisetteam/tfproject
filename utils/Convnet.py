import tensorflow as tf

###############################
# VARIABLE DEFINITION HELPERS #
###############################


def weight_variable(shape, normal_stddev=0.01):
    initializer = tf.truncated_normal_initializer(stddev=normal_stddev)
    return tf.get_variable(name='W', dtype=tf.float32, shape=shape, initializer=initializer, trainable=True)


def bias_variable(shape):
    initializer = tf.constant(0., shape=shape, dtype=tf.float32)
    return tf.get_variable(name='b', dtype=tf.float32, initializer=initializer, trainable=True)


############################
# LAYER DEFINITION HELPERS #
############################


def conv_layer(input, filter_size, num_filters, stride, name, padding='SAME'):
    with tf.variable_scope(name):
        num_channels = input.get_shape().as_list()[-1]

        # Define weight amd bias variables
        w = weight_variable(shape=[filter_size, filter_size, num_channels, num_filters])
        b = bias_variable(shape=[num_filters])

        # Define layer
        layer = tf.nn.conv2d(
            input=input,
            filter=w,
            strides=[1, stride, stride, 1],
            padding=padding)

        # Add bias
        layer += b

        return tf.nn.relu(layer)


def gdn_layer(input):
    return tf.contrib.layers.gdn(input)

def maxpool2d_layer(input, ksize, strides, name):
    return tf.layers.max_pooling2d(inputs=input, pool_size=(ksize, ksize), strides=1, name=name)

def maxpool_layer(input, ksize, stride, name, padding='SAME'):
    return tf.nn.max_pool(
        value=input,
        ksize=[1, ksize, ksize, 1],
        strides=[1, stride, stride, 1],
        padding=padding,
        name=name)


def flatten_layer(input, name='Flatten_Layer'):
    with tf.variable_scope(name):
        num_features = input.get_shape()[1:].num_elements()
        layer = tf.reshape(input, [-1, num_features])
    return layer


def fc_layer(input, num_units, name, use_relu=True):
    with tf.variable_scope(name):

        in_dim = input.get_shape()[1]
        W = weight_variable(shape=[in_dim, num_units])
        b = bias_variable(shape=[num_units])
        layer = tf.matmul(input, W)
        layer += b

        if use_relu:
            layer = tf.nn.relu(layer)


        return layer
