import tensorflow as tf
import numpy as np
import sys

from utils.Convnet import *
from utils.DatasetReader import load_cifar_100

import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="2"

###################
#    LOAD_DATA    #
###################
xtrain, ytrain, xtest, ytest = load_cifar_100('../cifar-100-python')

###################
#    DEFINE CNN   #
###################

img_w = xtrain.shape[1]
img_h = xtrain.shape[2]
n_chan = xtrain.shape[3]
n_classes = ytrain.shape[1]

tf.reset_default_graph()

with tf.name_scope("Input"):
    x = tf.placeholder(dtype=tf.float32, shape=[None, img_w, img_h, n_chan], name="X")
    y = tf.placeholder(dtype=tf.float32, shape=[None, n_classes], name="Y")

conv1 = tf.layers.conv2d(inputs=x, filters=32, kernel_size=3, name="Conv1", activation="relu")
gdn1 = tf.contrib.layers.gdn(conv1)
maxpool1 = tf.layers.max_pooling2d(inputs=gdn1, pool_size=2, strides=1, name="MaxPool1")
conv2 = tf.layers.conv2d(inputs=maxpool1, filters=64, kernel_size=3, name="Conv2", activation="relu")
gdn2 = tf.contrib.layers.gdn(conv2)
maxpool2 = tf.layers.max_pooling2d(inputs=gdn2, pool_size=2, strides=1, name="MaxPool2")
conv3 = tf.layers.conv2d(inputs=maxpool2, filters=128, kernel_size=3, name="Conv3", activation="relu")
gdn3 = tf.contrib.layers.gdn(conv3)
maxpool3 = tf.layers.max_pooling2d(inputs=gdn3, pool_size=2, strides=1, name="MaxPool3")
conv4 = tf.layers.conv2d(inputs=maxpool3, filters=256, kernel_size=3, name="Conv4", activation="relu")
gdn4 = tf.contrib.layers.gdn(conv4)
maxpool4 = tf.layers.max_pooling2d(inputs=gdn4, pool_size=2, strides=1, name="MaxPool4")

flatten = flatten_layer(input=maxpool4)

fc1 = fc_layer(input=flatten, num_units=256, name="fc1", use_relu=True)
fc2 = fc_layer(input=fc1, num_units=512, name="fc2", use_relu=True)
fc2_out = fc_layer(input=fc2, num_units=n_classes, name="fc2_out", use_relu=False)


#############################
#    DEFINE TF OPERATIONS   #
#############################

with tf.variable_scope("Train"):
    with tf.variable_scope("Predict"):
        predict = tf.argmax(tf.nn.sigmoid(fc2_out), axis=1, name='do_prediction')
    with tf.variable_scope('Loss'):
        loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=fc2_out, labels=y), name='loss')
    with tf.variable_scope('Accuracy'):
        is_correct_pred = tf.equal(predict, tf.argmax(y, axis=1))
        acc = tf.reduce_mean(tf.cast(is_correct_pred, dtype=tf.float32), name='accuracy')
    with tf.variable_scope("Optimizer"):
        optimizer = tf.train.AdamOptimizer(name='optimizer').minimize(loss)

with tf.name_scope('Init'):
    init = tf.global_variables_initializer()

#############################
#       TRAINING LOOP       #
#############################

PROJECTNAME = 'cifar_gdn'

def save_metrics(metricname, arr):
    with open(PROJECTNAME + '_' + metricname + '.txt', 'w') as f:
        for item in arr:
            f.write(str(item) + '\n')

EPOCHS = 200
BATCH_SIZE = 500

config = tf.ConfigProto()
config.gpu_options.allow_growth = True

accs_train = []
losses_train = []

accs_test = []
losses_test = []

with tf.Session(config=config) as sess:
    sess.run(tf.global_variables_initializer())
    summary_writer = tf.summary.FileWriter('./logs', sess.graph)
    for epoch in range(EPOCHS):
        accs_epoch = []
        losses_epoch = []
        for batch in range(xtrain.shape[0]//BATCH_SIZE):
            x_batch = xtrain[batch*BATCH_SIZE: (batch+1)*BATCH_SIZE]
            y_batch = ytrain[batch*BATCH_SIZE: (batch+1)*BATCH_SIZE]

            _, acc_batch, loss_batch = sess.run([optimizer, acc, loss], feed_dict={x: x_batch, y: y_batch})

            accs_epoch.append(acc_batch)
            losses_epoch.append(loss_batch)

            i = "Epoch {0} ({1}/{2})\tacc={3}\tloss={4}".format(epoch, batch, xtrain.shape[0]//BATCH_SIZE, np.mean(accs_epoch), np.mean(losses_epoch))
            sys.stdout.write("\r" + i)
            sys.stdout.flush()

        acc_mean_epoch = np.mean(accs_epoch)
        accs_train.append(acc_mean_epoch)

        loss_mean_epoch = np.mean(losses_epoch)
        losses_train.append(loss_mean_epoch)

        accs_test_batch = []
        losses_test_batch = []
        for batch in range(xtest.shape[0] // BATCH_SIZE):
            x_test_batch = xtrain[batch * BATCH_SIZE: (batch + 1) * BATCH_SIZE]
            y_test_batch = ytrain[batch * BATCH_SIZE: (batch + 1) * BATCH_SIZE]

            acc_test_batch, loss_test_batch = sess.run([tf.reduce_mean(acc), tf.reduce_mean(loss)], feed_dict={x: x_test_batch, y: y_test_batch})

            accs_test_batch.append(acc_test_batch)
            losses_test_batch.append(loss_test_batch)

        acc_test_mean = np.mean(accs_test_batch)
        loss_test_mean = np.mean(losses_test_batch)

        accs_test.append(acc_test_mean)
        losses_test.append(loss_test_mean)
        sys.stdout.write("\rFINISHED EPOCH {0}:\tacc={1}\tloss={2}\tacc_test={3}\tloss_test={4}\n".format(epoch, acc_mean_epoch, loss_mean_epoch, acc_test_mean, loss_test_mean))
        sys.stdout.flush()

        save_metrics("acc_train", accs_train)
        save_metrics("loss_train", losses_train)
        save_metrics("acc_test", accs_test)
        save_metrics("loss_test", losses_test)
